package cn.chowx.myjfinal.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具
 * @author zcqshine
 *
 */
public class StringUtil {
	/**
	 * 过滤特殊符号:<br>
	 * `~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？
	 * @param str
	 * @return
	 */
	public static String StringFilter(String str) {
		if(str != null){
			String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
			Pattern p = Pattern.compile(regEx);
			Matcher m = p.matcher(str);
			String result = m.replaceAll("");
			if(result != null)
				result = result.trim();
			return result;
		}
		return null;
	}
}
