package cn.chowx.myjfinal.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MD5签名, MD5签名校验,HMAC-SHA1
 * @author zcqshine
 *
 */
public class DigestUtil {
	private static final Logger LOG = LoggerFactory.getLogger(DigestUtil.class);
	private static final String HMAC_SHA1 = "HmacSHA1"; 
	/**
	 * 生成32位的MD5签名
	 * @param object
	 * @return String md5签名
	 */
	public static String Md5(String object) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(object.getBytes("utf-8"));
			byte b[] = md.digest();
			String HEX_CHARS = "0123456789abcdef";
			StringBuffer sb = new StringBuffer();
			for(byte aB : b){
				sb.append(HEX_CHARS.charAt(aB >>> 4 & 0x0F));
				sb.append(HEX_CHARS.charAt(aB & 0x0F));
			}
			
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			LOG.error("MD5签名失败",e);
			return null;
		} catch (UnsupportedEncodingException e) {
			LOG.error("MD5签名失败",e);
		}
		return null;
	}
	
	/**
	 * MD5签名校验,不区分签名的大小写
	 * @param oldSign	//旧的签名
	 * @param newStr	//要被验证的字符串
	 * @return boolean
	 */
	public static boolean check(String oldSign,String newStr){
		boolean flag = false;
		if(null != oldSign){
			int length = oldSign.length();
			if(length == 16){
				if(oldSign.equals(Md5(newStr).substring(8, 24))){
					flag = true;
				}
			}
			if(length == 32){
				if(oldSign.equalsIgnoreCase(Md5(newStr))){
					flag = true;
				}
			}
		}
		return flag;
	}
	
	/**
	 * HMAC-SHA1
	 * @param encryptText
	 * @param encryptKey
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public static byte[] getHMACSHA1(String encryptText, String encryptKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException{
		byte[] data = encryptText.getBytes("utf-8");
		byte[] key = encryptKey.getBytes("utf-8");
		SecretKeySpec signKey = new SecretKeySpec(key, HMAC_SHA1);
		Mac mac = Mac.getInstance(HMAC_SHA1);
		mac.init(signKey);
		return mac.doFinal(data);
	}
	
//	public static void main(String[] args) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException{
//		String url = URLEncoder.encode("/cp/game/getChannelList","utf-8");
//		StringBuilder sb = new StringBuilder();
//		sb.append("appID=").append("C55861CC8274089FCBEF187642E46BB4").append("&openID=").append("0f40475608212926e7d77842b7d51ebf").append("&platformType=").append(2);
//		String signStr = URLEncoder.encode(sb.toString(),"utf-8");
//		signStr = "GET&"+url+"&"+signStr;
//		byte[] sign = DigestUtil.getHMACSHA1(signStr, "e81d75dbe4878635945bc7979f526f0b&");
//		System.out.println(new String(sign));
//		sign = Base64.getEncoder().encode(sign);
//		System.out.println(URLEncoder.encode(new String(sign),"utf-8"));
//	}
	
	//http://open.gdatacube.net/cp/game/getChannelList?platformType=2&appID=C55861CC8274089FCBEF187642E46BB4&openID=0f40475608212926e7d77842b7d51ebf&sign=VLDfGVtkiCRFt6KpSIhGdBOZIqs%3D
}
