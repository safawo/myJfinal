package cn.chowx.myjfinal.help;

import java.util.List;

/**
 * 页面搜索过滤帮助类. 定义搜索字段的连接方法"AND"或者"OR", 定义搜索规则集 {@link PageSearchRulesHelp}
 * @author zcqshine
 *
 */
public class PageFilterHelp {
	private String groupOp;
	private List<PageSearchRulesHelp> rules;
	
	public String getGroupOp() {
		return groupOp;
	}
	public void setGroupOp(String groupOp) {
		this.groupOp = groupOp;
	}
	public List<PageSearchRulesHelp> getRules() {
		return rules;
	}
	public void setRules(List<PageSearchRulesHelp> rules) {
		this.rules = rules;
	}
}

