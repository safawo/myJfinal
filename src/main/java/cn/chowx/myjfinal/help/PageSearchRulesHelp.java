package cn.chowx.myjfinal.help;

/**
 * 页面搜索规则. 定义了搜索的字段名称,操作方式,字段值.
 * @author zcqshine
 *
 */
public class PageSearchRulesHelp {
	private String field;
	private String op;
	private String data;
	
	/**
	 * 
	 * @param field 搜索的字段名
	 * @param op	比较方式,eq,lt,gt	
	 * @param data
	 */
//	public PageSearchRulesHelp(String field,String op, String data){
//		this.field = field;
//		this.op = op;
//		this.data = data;
//	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
