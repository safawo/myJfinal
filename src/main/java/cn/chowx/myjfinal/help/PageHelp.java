package cn.chowx.myjfinal.help;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import cn.chowx.myjfinal.util.StringUtil;

import com.google.common.base.Strings;

/**
 * 分页,搜索助手类
 * @author zcqshine
 *
 */
public class PageHelp {
	public static final String OPER_CREATE = "add";		//添加
	public static final String OPER_UPDATE =  "edit";	//更新
	public static final String OPER_DELETE = "del";		//删除
	public static final String ORDER_ASC = "asc";		//升序排列
	public static final String ORDER_DESC = "desc";		//降序排列
	
	public static final String OP_EQUELS = "eq";		//等于
	public static final String OP_NOT_EQUELS = "ne";	//不等于
	public static final String OP_LESS_THAN = "lt";		//小于
	public static final String OP_GREAT_THAN = "gt";	//大于
	public static final String OP_LESS_EQUELS = "le";	//小于等于
	public static final String OP_GREAT_EQUELS = "ge";	//大于等于
	public static final String OP_IN = "in";			//属于
	public static final String OP_NOT_IN = "ni";		//不属于
	
	public static final String GROUPOP_AND = "and";		//AND 连接		
	public static final String GROUPOP_OR = "or";       //OR 连接
	
	private boolean search;	//是否搜索
	private long nd ;		//随机数
	private int rows;		//每页显示的行数
	private int pageNum;	//页数
	private String sidx;	//排序列名
	private String sord;	//排序方式 acs 或者 desc
	private PageFilterHelp filter;	//多字段搜索的时候
	private String oper; 	//CRUD 操作. 
	
	//单字段搜索基本不用了,多字段搜索已经覆盖了单字段搜索
	private String searchField;		//字段名. 单字段搜索时候
	private String searchString;	//字段值. 单字段搜索时候
	private String searchOper;		//操作方式.(等于,小于,大于,小于等于,大于等于,属于,不属于) 单字段搜索时候
	
	/**
	 * 拼接查询条件
	 * @param pageHelp
	 * @param tableAlias 表的别名, 如果没有设置为 null 即可
	 * @return
	 */
	public static String buildSearchSQL(PageHelp pageHelp,String tableAlias){
		StringBuffer sb = new StringBuffer();
		if(pageHelp.isSearch()){
			PageFilterHelp filter = pageHelp.getFilter();
			String groupOp = filter.getGroupOp();
			if(filter != null){
				List<PageSearchRulesHelp> rules = filter.getRules();
				if(rules != null){
					for(PageSearchRulesHelp rule : rules){
						String field = rule.getField();
						String data = rule.getData();
						if(!NumberUtils.isNumber(data)){
							if(!Strings.isNullOrEmpty(data)){
								data = StringUtil.StringFilter(data);
							}
							data = "'" + data + "'";
						}
						String op = rule.getOp();
						if(tableAlias != null && tableAlias.trim().length() > 0){
							sb.append(" ").append(groupOp).append(" ").append(tableAlias).append(".").append(field).append("");
						}else{
							sb.append(" ").append(groupOp).append(" `").append(field).append("`");
						}
						
						switch (op) {
						case PageHelp.OP_EQUELS: 
							sb.append(" = ").append(data);
							break;
						case PageHelp.OP_NOT_EQUELS:
							sb.append(" != ").append(data);
							break;
						case PageHelp.OP_LESS_THAN:
							sb.append(" < ").append(data);
							break;
						case PageHelp.OP_LESS_EQUELS:
							sb.append("<= ").append(data);
							break;
						case PageHelp.OP_GREAT_THAN:
							sb.append(" > ").append(data);
							break;
						case PageHelp.OP_GREAT_EQUELS:
							sb.append(" >= ").append(data);
							break;
						case PageHelp.OP_IN:
							sb.append(" in ( ").append(data).append(")");
							break;
						case PageHelp.OP_NOT_IN:
							sb.append(" not in (").append(data).append(")");
							break;
						default:
							break;
						}
					}
				}
			}
		}
		return sb.toString();
	}
	
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public String getSearchOper() {
		return searchOper;
	}
	public void setSearchOper(String searchOper) {
		this.searchOper = searchOper;
	}
	public PageFilterHelp getFilter() {
		return filter;
	}
	public void setFilter(PageFilterHelp filter) {
		this.filter = filter;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public long getNd() {
		return nd;
	}
	public void setNd(long nd) {
		this.nd = nd;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	
	
}

