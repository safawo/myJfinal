package cn.chowx.myjfinal.help;

/**
 * 返回到客户端的json帮助类
 * @author zcqshine
 *
 */
public class JsonResponseHelp {
	public static final int FLAG_SUCCESS = 1;
	public static final int FLAG_FAILURE = 0;
	private int flag ;
	private String msg;
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
