package cn.chowx.myjfinal.common;

import java.util.Map.Entry;
import java.util.Properties;

import cn.chowx.myjfinal.baseinfo.ChannelController;
import cn.chowx.myjfinal.baseinfo.CoinController;
import cn.chowx.myjfinal.baseinfo.CountryController;
import cn.chowx.myjfinal.baseinfo.OperatorController;
import cn.chowx.myjfinal.baseinfo.ProductConroller;
import cn.chowx.myjfinal.baseinfo.SpInfoController;
import cn.chowx.myjfinal.baseinfo.TariffCountryController;
import cn.chowx.myjfinal.baseinfo.model.Channel;
import cn.chowx.myjfinal.baseinfo.model.Coin;
import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.baseinfo.model.Operator;
import cn.chowx.myjfinal.baseinfo.model.Product;
import cn.chowx.myjfinal.baseinfo.model.SpInfo;
import cn.chowx.myjfinal.baseinfo.model.TariffCountry;
import cn.chowx.myjfinal.index.IndexController;
import cn.chowx.myjfinal.shiro.MyJdbcAuthzService;
import cn.chowx.myjfinal.statistics.RegisterActiveController;
import cn.chowx.myjfinal.statistics.SmsFeeRateController;
import cn.chowx.myjfinal.statistics.model.RegisterActive;
import cn.chowx.myjfinal.statistics.model.SmsFeeRate;
import cn.chowx.myjfinal.system.SysActionController;
import cn.chowx.myjfinal.system.SysRoleController;
import cn.chowx.myjfinal.system.SysUserController;
import cn.chowx.myjfinal.system.model.SysAction;
import cn.chowx.myjfinal.system.model.SysRole;
import cn.chowx.myjfinal.system.model.SysRoleAction;
import cn.chowx.myjfinal.system.model.SysUser;
import cn.chowx.myjfinal.system.model.SysUserRole;
import cn.dreampie.shiro.core.ShiroInterceptor;
import cn.dreampie.shiro.core.ShiroPlugin;

import com.alibaba.druid.wall.WallFilter;
import com.google.common.base.Strings;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
//import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.render.FreeMarkerRender;

import freemarker.template.TemplateModelException;

public class MyJFinalConfig extends JFinalConfig {
	private Routes routes;
	private final static Logger LOG = Logger.getLogger(MyJFinalConfig.class);
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
		me.setBaseViewPath("/WEB-INF");
	}

	@Override
	public void configRoute(Routes me) {
		this.routes = me;
		me.add("/",IndexController.class,"/");					// 第三个参数为该Controller的视图存放路径
		me.add("/user",SysUserController.class);				//用户	 第三个参数省略时默认与第一个参数值相同
		me.add("/action",SysActionController.class,"/user");	//资源权限
		me.add("/role",SysRoleController.class,"/user");		//角色
		me.add("/spInfo",SpInfoController.class,"/baseInfo"); 	//SP 信息相关
		me.add("/country",CountryController.class,"/baseInfo");	//国家|地区信息
		me.add("/coin",CoinController.class, "/baseInfo");    	//币种信息
		me.add("/operator",OperatorController.class,"/baseInfo"); //运营商信息
		me.add("/tariff",TariffCountryController.class,"/baseInfo"); //资费国家配置
		me.add("/product",ProductConroller.class, "/baseInfo");		//产品信息
		me.add("/channel",ChannelController.class,"/baseInfo");		//渠道信息
		me.add("/regAct",RegisterActiveController.class,"/statistics"); //激活留存统计
		me.add("/smsFeeRate",SmsFeeRateController.class,"/statistics"); //短信成功率,付费率等统计
		
	}

	@Override
	public void configPlugin(Plugins me) {
		Prop jdbc = PropKit.use("jdbc.properties");
		DruidPlugin druidPlugin = new DruidPlugin(jdbc.get("jdbcUrl"), jdbc.get("user"), jdbc.get("password").trim());
		WallFilter wallFilter = new WallFilter();
		wallFilter.setDbType("mysql");
		druidPlugin.addFilter(wallFilter);
		me.add(druidPlugin);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);
		arp.addMapping(TableName.SysUser.getTableName(), SysUser.class);
		arp.addMapping(TableName.SysRole.getTableName(), SysRole.class);
		arp.addMapping(TableName.SysUserRole.getTableName(), SysUserRole.class);
		arp.addMapping(TableName.SysAction.getTableName(), SysAction.class);
		arp.addMapping(TableName.SysRoleAction.getTableName(), SysRoleAction.class);
		
		arp.addMapping(TableName.SpInfo.getTableName(), SpInfo.class);
		arp.addMapping(TableName.Country.getTableName(), Country.class);
		arp.addMapping(TableName.Coin.getTableName(), Coin.class);
		arp.addMapping(TableName.Operator.getTableName(), Operator.class);
		arp.addMapping(TableName.TariffCountry.getTableName(), TariffCountry.class);
		arp.addMapping(TableName.Product.getTableName(), Product.class);
		arp.addMapping(TableName.Channel.getTableName(), Channel.class);
		arp.addMapping(TableName.RegisterActive.getTableName(),RegisterActive.class);
		arp.addMapping(TableName.StatisticsSmsFee.getTableName(), SmsFeeRate.class);
		
		me.add(new ShiroPlugin(routes, new MyJdbcAuthzService()));
		me.add(new EhCachePlugin()); 
		
//		RedisPlugin opRedis = new RedisPlugin("operator", "localhost");
//		me.add(opRedis);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new ShiroInterceptor());
	}

	/**
	 * 项目路径
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("basePath"));
	}
	
	/**
	 * 设置freemarker的全局变量
	 */
	public void afterJFinalStart(){
		super.afterJFinalStart();
		Properties p = loadPropertyFile("freemaker.properties");
		FreeMarkerRender.setProperties(p);
		Properties values = loadPropertyFile("freemarkerValues.properties");
		if(values != null && !values.isEmpty()){
			for(Entry<Object,Object> entry : values.entrySet()){
				String key = entry.getKey() == null ? null : (String)entry.getKey();
				String value = entry.getValue() == null ? null : (String) entry.getValue();
				if(!Strings.isNullOrEmpty(key) && !Strings.isNullOrEmpty(value)){
					try {
						FreeMarkerRender.getConfiguration().setSharedVariable(key, value);
					} catch (TemplateModelException e) {
						LOG.error("加载freemarker全局变量失败.{}",e);
					}
				}
			}
		}
		
//		FreeMarkerRender.getConfiguration().setSharedVariable("shiro", new ShiroTags());
	}

}
