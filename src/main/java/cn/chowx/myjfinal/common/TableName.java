package cn.chowx.myjfinal.common;

/**
 * 类--表名映射关系. 通过类名获取表名的方法为:TableName.XXX.getName; XXX 为类名
 * @author zcqshine
 *
 */
public enum TableName {
	SysUser("sys_user",1),
	SysAction("sys_action",2),
	SysRole("sys_role",3),
	SysRoleAction("sys_role_action",4),
	SysUserRole("sys_user_role",5),
	
	SpInfo("u_t_spInfos",6),
	Country("u_t_country",7),		//国家
	Coin("u_t_coin",8),				//币种
	Operator("u_t_operator",9),		//运营商
	TariffCountry("u_t_tariff_country",10), //计费国家
	Product("u_t_product",11), 		//产品
	StatisticsSmsFee("u_t_statistics_sms_fee",12), //统计短信,费用等
	RegisterActive("u_t_register_active",13), //注册激活数据
	Channel("u_t_channel",14);		//渠道
	
	private String tableName;
	private int index;
	
	private TableName(String tableName, int index){
		this.tableName = tableName;
		this.index = index;
	}


	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
}
