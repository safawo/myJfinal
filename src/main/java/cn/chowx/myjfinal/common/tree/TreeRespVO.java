package cn.chowx.myjfinal.common.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据返回对象
 * @author zcqshine
 *
 */
public class TreeRespVO {
	private List<Node> data = new ArrayList<>();

	public List<Node> getData() {
		return data;
	}

	public void setData(List<Node> data) {
		this.data = data;
	}
	
}
