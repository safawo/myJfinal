package cn.chowx.myjfinal.common.tree;
/**
 * 节点信息
 * @author zcqshine
 *
 */
public class Node {
	private int id;			
	private String name;	//节点名称
	private int pId;		//父节点 ID
	private boolean open;	//是否展开
	private boolean checked;	//是否勾选
	private boolean nocheck;	//是否有勾选框
	
	private int uid;	//用户 id
	private int roleId;	//角色 id
	
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPId() {
		return pId;
	}
	public void setPId(int pId) {
		this.pId = pId;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isNocheck() {
		return nocheck;
	}
	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}
	
	
}
