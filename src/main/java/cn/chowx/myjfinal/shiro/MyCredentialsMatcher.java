package cn.chowx.myjfinal.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

import cn.chowx.myjfinal.util.DigestUtil;
/**
 * shiro中使用MD5加密
 * @author zcqshine
 *
 */
public class MyCredentialsMatcher extends SimpleCredentialsMatcher {
	
	@Override
	public boolean doCredentialsMatch(AuthenticationToken authcToken, AuthenticationInfo info){
		UsernamePasswordToken token = (UsernamePasswordToken)authcToken;
		Object tokenCredentials = DigestUtil.Md5(String.valueOf(token.getPassword()));
		Object accountredentials = getCredentials(info);
		return equals(tokenCredentials, accountredentials);
	}
}
