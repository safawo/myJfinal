package cn.chowx.myjfinal.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import cn.chowx.myjfinal.system.model.SysUser;

/**
 * 当前登录用户 SESSION
 * @author zcqshine
 *
 */
public class UserSeesion {
	/**
	 * 获取当前登录用户信息
	 * @return
	 */
	public static SysUser getCurrentUser(){
		Subject currentUser = SecurityUtils.getSubject();
		SysUser user = (SysUser)currentUser.getPrincipal();
		return user;
	}
}
