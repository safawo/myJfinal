package cn.chowx.myjfinal.system;

import cn.chowx.myjfinal.system.model.SysUser;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * SysUserValidator
 * @author zcqshine
 *
 */
public class SysUserValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRequiredString("sysUser.account", "accountMsg", "请输入登录帐号");
		validateRequiredString("sysUser.password", "passwordMsg", "请输入密码");
	}

	@Override
	protected void handleError(Controller c) {
		c.keepModel(SysUser.class);
		String actionKey = getActionKey();
		if(actionKey.equals("/user/add")){
			c.render("add.html");
		}else if(actionKey.equals("/user/update")){
			c.render("edit.html");
		}

	}

}
