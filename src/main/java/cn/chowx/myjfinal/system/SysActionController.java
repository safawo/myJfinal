package cn.chowx.myjfinal.system;

import org.apache.commons.lang3.math.NumberUtils;

import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;
import cn.chowx.myjfinal.system.model.SysAction;

import com.google.common.base.Strings;
//import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * 资源权限controller
 * @author zcqshine
 *
 */
public class SysActionController extends BaseController {
//	private static final Logger log = Logger.getLogger(SysActionController.class);
	public void index(){
		String actionKeyValue = SysAction.DAO.getSelectKeyValue();
		setAttr("actionKeyValue", actionKeyValue);
		render("actions.html");
	}
	
	public void list(){
		super.setPageHelp();
		Page<Record> page = SysAction.DAO.findRecordList(pageHelp);
		renderJsonData(page);
	}
	
	/**
	 * 创建,更新,删除权限资源
	 */
	public void cud(){
		String oper = getPara("oper");
		
		int id = NumberUtils.toInt(getPara("id"));
		String name = getPara("name");
		int parentId = NumberUtils.toInt(getPara("parentId"));
		String url = getPara("url");
		String value = getPara("value");
		int order = NumberUtils.toInt(getPara("order"));
		int status = NumberUtils.toInt(getPara("status"));
		
		SysAction action = new SysAction();
		action.set("id", id);
		action.set("name", name);
		action.set("parentId", parentId);
		action.set("url", url);
		action.set("value", value);
		action.set("order", order);
		action.set("status", status);
		
		String msg = null;
		
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(action);
				break;
			case PageHelp.OPER_UPDATE: 
				msg = update(action);
				break;
			case PageHelp.OPER_DELETE:
				msg = delete(action);
				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(SysAction action){
		boolean flag = action.save();
		if(flag){
			return "添加权限资源成功";
		}else{
			return "添加权限资源失败";
		}
	}
	
	private String update(SysAction action){
		boolean flag = action.update();
		if(flag){
			return "更新权限资源成功";
		}else{
			return "更新权限资源失败";
		}
	}
	
	private String delete(SysAction action){
		boolean flag = action.delete();
		if(flag){
			return "删除权限资源成功";
		}else{
			return "删除权限资源失败";
		}
	}
	
}
