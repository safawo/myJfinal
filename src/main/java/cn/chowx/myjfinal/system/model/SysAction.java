package cn.chowx.myjfinal.system.model;

import java.util.ArrayList;
import java.util.List;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.common.tree.Node;
import cn.chowx.myjfinal.help.PageHelp;

import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * 系统资源. 实现了Comparable接口，按照 order 参数来进行升序排序。 
 * @author zcqshine
 *
 */
public class SysAction extends BaseModel<SysAction> implements Comparable<SysAction>{
	private static final long serialVersionUID = -3647614592230026454L;
	public static final SysAction DAO = new SysAction();
	private static int STATUS_ENABLE = 1;	//有效
//	private static int STATUS_DISABLE = 0;	//无效
	
	@Override
	public int compareTo(SysAction action) {
		return this.getInt("order").compareTo(action.getInt("order"));
	}
	
	/**
	 * 分页查询,搜索
	 * @param pageHelp
	 * @return Page
	 */
	public Page<SysAction> findAll(PageHelp pageHelp){
		return super.findPage(pageHelp, TableName.SysAction.getTableName());
	}
	
	/**
	 * 根据资源状态获取资源列表.
	 * @param status 1-有效资源,0-无效资源
	 * @return
	 */
	public List<SysAction> findList(int status){
		return find("select * from " + TableName.SysAction.getTableName() + " WHERE status = ?",status);
	}
	
	/**
	 * 分页查询,搜索.
	 * @param pageHelp
	 * @return
	 */
	public Page<Record> findRecordList(PageHelp pageHelp){
		String select = "select A.*, B.name parentName";
		StringBuilder sb = new StringBuilder("FROM sys_action A, sys_Action B where A.parentId = B.id ");
		sb.append(PageHelp.buildSearchSQL(pageHelp, "A"));
		sb.append(" order by A.parentId, A.order asc");
		Page<Record> records = Db.paginate(pageHelp.getPageNum(), pageHelp.getRows(), select, sb.toString());
		return records;
	}
	
	/**
	 * 获取资源 id:name;id:name... 的字符串
	 * @return
	 */
	public String getSelectKeyValue(){
		List<SysAction> actions = findList(STATUS_ENABLE);
		StringBuilder sb = new StringBuilder();
		if(actions != null){
			for(SysAction action : actions){
				sb.append(action.getInt("id")).append(":")
				.append(action.getStr("name")).append(";");
			}
			if(sb != null){
				sb = sb.deleteCharAt(sb.lastIndexOf(";"));
			}
		}
		return sb.toString();
	}
	
	/**
	 * 根据角色 ID 获取资源列表,如果该角色拥有某一资源,则设置为勾选状态.最后转换为 json 格式返回.
	 * @param roleId
	 * @return String
	 */
	public String getActionJson(int roleId){
		List<SysAction> hasAction = getSysActionByRoleId(roleId);
		List<SysAction> allAction = findList(STATUS_ENABLE);
		List<Node> nodes = new ArrayList<>();
		for(SysAction action : allAction){
			Node node = new Node();
			node.setRoleId(roleId);
			node.setId(action.getInt("id"));
			node.setName(action.getStr("name"));
			node.setOpen(true);
			if(hasAction.contains(action)){
				node.setChecked(true);
			}
			node.setPId(action.getInt("parentId"));
			nodes.add(node);
		}
		return JsonKit.toJson(nodes);
	}
	
	/**
	 * 根据角色 id 获取该角色拥有的有效资源
	 * @param roleId
	 * @return
	 */
	private List<SysAction> getSysActionByRoleId(int roleId){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ")
			.append(TableName.SysAction.getTableName())
			.append(" WHERE id in (select actionId from ")
			.append(TableName.SysRoleAction.getTableName())
			.append(" WHERE roleId = ? and status = 1) and status = ").append(STATUS_ENABLE).append(" ORDER BY id ASC");
		List<SysAction> list = find(sb.toString(), roleId);
		return list;
	}
}
