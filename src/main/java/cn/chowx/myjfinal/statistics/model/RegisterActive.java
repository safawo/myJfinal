package cn.chowx.myjfinal.statistics.model;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageFilterHelp;
import cn.chowx.myjfinal.help.PageHelp;
import cn.chowx.myjfinal.help.PageSearchRulesHelp;
import cn.chowx.myjfinal.util.StringUtil;

public class RegisterActive extends BaseModel<RegisterActive>{
	private static final long serialVersionUID = -2245187547350330129L;
	public static RegisterActive DAO = new RegisterActive();
	
	/**
	 * 分页获取渠道激活数据
	 * @param pageHelp
	 * @return
	 */
	public Page<Record> findList(PageHelp pageHelp){
		String select = "SELECT r.* , c.name countryName, ch.name channelName, pr.name productName ";
		StringBuilder sb = new StringBuilder();
		sb.append("FROM ").append(TableName.RegisterActive.getTableName()).append(" r")
			.append(" LEFT JOIN ").append(TableName.Country.getTableName()).append(" c ON r.countryId = c.id")
			.append(" LEFT JOIN ").append(TableName.Channel.getTableName()).append(" ch ON r.channelId = ch.id")
			.append(" LEFT JOIN ").append(TableName.Product.getTableName()).append(" pr ON r.productId = pr.id")
			.append(" WHERE 1=1 ");
		if(pageHelp.isSearch()){
			PageFilterHelp filter = pageHelp.getFilter();
			String groupOp = filter.getGroupOp();
			if(filter != null){
				List<PageSearchRulesHelp> rules = filter.getRules();
				if(rules != null){
					for(PageSearchRulesHelp rule : rules){
						String field = rule.getField();
						String data = rule.getData();
						if(!NumberUtils.isNumber(data)){
							if(!Strings.isNullOrEmpty(data)){
								data = StringUtil.StringFilter(data);
							}
							data = "'" + data + "'";
						}
						String op = rule.getOp();
						sb.append(" ").append(groupOp).append(" A.").append(field);
						
						switch (op) {
						case PageHelp.OP_EQUELS: 
							sb.append(" = ").append(data);
							break;
						case PageHelp.OP_NOT_EQUELS:
							sb.append(" != ").append(data);
							break;
						case PageHelp.OP_LESS_THAN:
							sb.append(" < ").append(data);
							break;
						case PageHelp.OP_LESS_EQUELS:
							sb.append("<= ").append(data);
							break;
						case PageHelp.OP_GREAT_THAN:
							sb.append(" > ").append(data);
							break;
						case PageHelp.OP_GREAT_EQUELS:
							sb.append(" >= ").append(data);
							break;
						case PageHelp.OP_IN:
							sb.append(" in ( ").append(data).append(")");
							break;
						case PageHelp.OP_NOT_IN:
							sb.append(" not in (").append(data).append(")");
							break;
						default:
							break;
						}
					}
				}
			}
		}
		if(!Strings.isNullOrEmpty(pageHelp.getSidx())){
			sb.append(" ORDER BY r.").append(pageHelp.getSidx()).append(" ").append(pageHelp.getSord());
		}else{
			sb.append(" ORDER BY r.separatorDate,r.channelId,r.productId,r.countryId DESC");
		}
		Page<Record> records = Db.paginate(pageHelp.getPageNum(), pageHelp.getRows(), select, sb.toString());
		return records;
	}
}
