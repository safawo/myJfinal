package cn.chowx.myjfinal.statistics;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.baseinfo.model.Channel;
import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.baseinfo.model.Product;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.statistics.model.RegisterActive;

/**
 * 渠道激活留存数据 Controller
 * @author zcqshine
 *
 */
public class RegisterActiveController extends BaseController {
	/**
	 * 跳转到激活数据页面
	 */
	public void toRegList(){
		String countryKeyValues = Country.DAO.getKeyValues();
		String channelKeyValues = Channel.DAO.getKeyValues();
		String productKeyValues = Product.DAO.getKeyValues();
		setAttr("countryKeyValues",countryKeyValues);
		setAttr("channelKeyValues",channelKeyValues);
		setAttr("productKeyValues",productKeyValues);
		render("register.html");
	}
	
	/**
	 * 跳转到留存数据页面
	 */
	public void toActList(){
		String countryKeyValues = Country.DAO.getKeyValues();
		String channelKeyValues = Channel.DAO.getKeyValues();
		String productKeyValues = Product.DAO.getKeyValues();
		setAttr("countryKeyValues",countryKeyValues);
		setAttr("channelKeyValues",channelKeyValues);
		setAttr("productKeyValues",productKeyValues);
		render("active.html");
	}
	
	/**
	 * 分页获取激活,留存数据
	 */
	public void  list(){
		setPageHelp();
		Page<Record> page = RegisterActive.DAO.findList(pageHelp);
		renderJsonData(page);
	}
	
}
