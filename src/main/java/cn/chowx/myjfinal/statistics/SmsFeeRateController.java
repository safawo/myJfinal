package cn.chowx.myjfinal.statistics;

import org.apache.commons.lang3.math.NumberUtils;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.baseinfo.model.Channel;
import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.baseinfo.model.Operator;
import cn.chowx.myjfinal.baseinfo.model.Product;
import cn.chowx.myjfinal.baseinfo.model.SpInfo;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.statistics.model.SmsFeeRate;

/**
 * 短信发送,计费等信息
 * @author zcqshine
 *
 */
public class SmsFeeRateController extends BaseController {
	/**
	 * 跳转到 SP 成功率查询页面
	 */
	public void toSpOkRateList(){
		 String countryKeyValues = Country.DAO.getKeyValues();
		 setAttr("countryKeyValues",countryKeyValues);
		 render("spSuccessRateList.html");
	 }
	
	/**
	 * sp成功率页面. 以国家来分
	 */
	public void spRateList(){
		setPageHelp();
		Page<Record> page = SmsFeeRate.DAO.findCountryRecordList(pageHelp);
		renderJsonData(page);
	}
	
	/**
	 * 跳转到某国家下,各运营商的数据
	 */
	public void toOperRateList(){
		int countryId = NumberUtils.toInt(getPara("cid"));
		String operKeyValues = Operator.DAO.getSelectKeyValue();
		setAttr("countryId", countryId);
		setAttr("operKeyValues", operKeyValues);
		render("operatorRateList.html");
	}
	
	/**
	 * 国家下各运营商下的数据
	 */
	public void oprateList(){
		int countryId = NumberUtils.toInt(getPara("cid"));
		setPageHelp();
		Page<Record> page = SmsFeeRate.DAO.findCountryProviderRecordList(pageHelp,countryId);
		renderJsonData(page);
	}
	
	/**
	 * 跳转到渠道成功率列表页面
	 */
	public void toChRateList(){
		String countryKeyValues = Country.DAO.getKeyValues();
		String channelKeyValues = Channel.DAO.getKeyValues();
		String productKeyValues = Product.DAO.getKeyValues();
		String spKeyValues = SpInfo.DAO.getKeyValues();
		setAttr("countryKeyValues",countryKeyValues);
		setAttr("channelKeyValues",channelKeyValues);
		setAttr("productKeyValues",productKeyValues);
		setAttr("spKeyValues",spKeyValues);
		render("channelRateList.html");
	}
	
	/**
	 * 渠道成功率列表页面
	 */
	public void chRateList(){
		setPageHelp();
		Page<Record> page = SmsFeeRate.DAO.findChannelRecordList(pageHelp);
		renderJsonData(page);
	}
}
