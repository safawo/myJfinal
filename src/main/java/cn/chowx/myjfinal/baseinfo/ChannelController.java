package cn.chowx.myjfinal.baseinfo;

import java.util.Calendar;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;

import cn.chowx.myjfinal.baseinfo.model.Channel;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;
import cn.chowx.myjfinal.shiro.UserSeesion;

/**
 * 渠道 controller
 * @author zcqshine
 *
 */
public class ChannelController extends BaseController {
	public void index(){
		render("channel.html");
	}
	
	/**
	 * 分页获取渠道列表
	 */
	public void list(){
		setPageHelp();
		String pageJson = Channel.DAO.findAll(pageHelp);
		renderJson(pageJson);
	}
	
	/**
	 * 新增,更新,删除操作
	 */
	public void cud(){
		String oper = getPara("oper");
		int id = NumberUtils.toInt(getPara("id"));
		String name = getPara("name");
		String remark = getPara("remark");
		String contact = getPara("contact");
		String address = getPara("address");
		String phone = getPara("phone");
		String email = getPara("email");
		int status = NumberUtils.toInt(getPara("status"));
		
		Channel channel = new Channel();
		channel.set("id", id);
		channel.set("name", name);
		channel.set("remark",remark);
		channel.set("contact", contact);
		channel.set("address", address);
		channel.set("phone", phone);
		channel.set("email", email);
		channel.set("status", status);
		
		String msg = null;
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(channel);
				break;
			case PageHelp.OPER_UPDATE:
				msg = update(channel);
				break;
//			case PageHelp.OPER_DELETE:
//				msg = delete(product);
//				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(Channel channel){
		channel.set("createUid", UserSeesion.getCurrentUser().getInt("id"));
		channel.set("createTime", Calendar.getInstance().getTime());
		return channel.save()? "添加成功":"添加失败";
	}
	
	private String update(Channel channel){
		return channel.update()?"更新成功":"更新失败";
	}
}
