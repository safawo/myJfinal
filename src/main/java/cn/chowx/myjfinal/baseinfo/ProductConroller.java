package cn.chowx.myjfinal.baseinfo;

import java.util.Calendar;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;

import cn.chowx.myjfinal.baseinfo.model.Product;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;
import cn.chowx.myjfinal.shiro.UserSeesion;

/**
 * 产品 Controller
 * @author zcqshine
 *
 */
public class ProductConroller extends BaseController {
	public void index(){
		render("product.html");
	}
	
	/**
	 * 分页获取产品列表
	 */
	public void list(){
		setPageHelp();
		String pagejson = Product.DAO.findAll(pageHelp);
		renderJson(pagejson);
	}
	
	/**
	 * 创建,更新,删除操作
	 */
	public void cud(){
		String oper = getPara("oper");
		
		int id = NumberUtils.toInt(getPara("id"));
		String name = getPara("name");		//产品名称
		String remarks = getPara("remarks"); //产品备注
		String pUrl = getPara("pUrl");	//产品网址
		String version = getPara("version"); //产品版本
		int status = NumberUtils.toInt(getPara("status"));
		
		Product product = new Product();
		product.set("id", id);
		product.set("name", name);
		product.set("remarks", remarks);
		product.set("pUrl", pUrl);
		product.set("version",version);
		product.set("status",status);
		
		String msg = null;
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(product);
				break;
			case PageHelp.OPER_UPDATE:
				msg = update(product);
				break;
//			case PageHelp.OPER_DELETE:
//				msg = delete(product);
//				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(Product product){
		product.set("createUid", UserSeesion.getCurrentUser().getInt("id"));
		product.set("createTime",Calendar.getInstance().getTime());
		return product.save()? "添加成功":"添加失败";
	}
	
	private String update(Product product){
		return product.update() ? "更新成功" : "更新失败";
	}
	
	
}
