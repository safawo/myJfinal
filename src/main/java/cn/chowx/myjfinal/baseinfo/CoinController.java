package cn.chowx.myjfinal.baseinfo;


/**
 * 币种 controller
 */
import org.apache.commons.lang3.math.NumberUtils;

import cn.chowx.myjfinal.baseinfo.model.Coin;
import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/**
 * 币种 controller
 * @author zcqshine
 *
 */
public class CoinController extends BaseController {
	public void index(){
		String countryKeyValues = Country.DAO.getKeyValues();
		setAttr("countryKeyValues", countryKeyValues);
		render("coin.html");
	}
	
	/**
	 * 分页获取币种信息
	 */
	public void list(){
		setPageHelp();
		Page<Record> page = Coin.DAO.findRecordList(pageHelp);
		renderJsonData(page);
	}
	
	public void cud(){
		String oper = getPara("oper");
		
		int id = NumberUtils.toInt(getPara("id"));
		String name = getPara("name");
		int countryId = NumberUtils.toInt(getPara("country"));
		String acronym = getPara("acronym");  //缩写
		
		Coin coin = new Coin();
		coin.set("id", id);
		coin.set("name", name);
		coin.set("country", countryId);
		coin.set("acronym", acronym);
		
		String msg = null;
		
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(coin);
				break;
			case PageHelp.OPER_UPDATE: 
				msg = update(coin);
				break;
			case PageHelp.OPER_DELETE:
//				msg = delete(coin);
				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(Coin coin){
		return coin.save() ? "添加成功" : "添加失败";
	}
	
	private String update(Coin coin){
		return coin.update() ? "更新成功" : "更新失败";
	}
	
//	private String delete(Coin coin){
//		return coin.delete() ? "删除成功" : "删除失败";
//	}
}
