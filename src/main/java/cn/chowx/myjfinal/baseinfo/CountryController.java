package cn.chowx.myjfinal.baseinfo;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;

import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 国家/地区信息 controller
 * @author zcqshine
 *
 */
public class CountryController extends BaseController {
	public void index(){
		render("country.html");
	}
	
	public void list(){
		setPageHelp();
		String pagejson = Country.DAO.findAll(pageHelp);
		renderJson(pagejson);
	}
	
	/**
	 * 增删改操作
	 */
	public void cud(){
		String oper =  getPara("oper");
		String name = getPara("name");
		int id = NumberUtils.toInt(getPara("id"));
		
		Country country = new Country();
		country.set("id",id);
		country.set("name", name);
		
		String msg = null;
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(country);
				break;
			case PageHelp.OPER_UPDATE:
				msg = update(country);
				break;
			case PageHelp.OPER_DELETE:
				msg = delete(country);
				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(Country country){
		if(country.save()){
			return "添加成功";
		}else{
			return "添加失败";
		}
	}
	
	private String update(Country country){
		if(country.update()){
			return "更新成功";
		}else{
			return "更新失败";
		}
	}
	
	private String delete(Country country){
		if(country.delete())
			return "删除成功";
		else
			return "删除失败";
	}
	
	
}
