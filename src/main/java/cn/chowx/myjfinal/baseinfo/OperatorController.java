package cn.chowx.myjfinal.baseinfo;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.baseinfo.model.Operator;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 运营商 controller
 * @author zcqshine
 *
 */
public class OperatorController extends BaseController {
	public void index(){
		String countryKeyValues = Country.DAO.getKeyValues();
		setAttr("countryKeyValues", countryKeyValues);
		render("operator.html");
	}
	
	/**
	 * 分页获取运营商信息
	 */
	public void list(){
		setPageHelp();
		Page<Record> page = Operator.DAO.findRecordList(pageHelp);
		renderJsonData(page);
	}
	
	/**
	 * 增删改 操作
	 */
	public void cud(){
		String oper = getPara("oper");
		int id = NumberUtils.toInt(getPara("id"));
		String name = getPara("name");
		int country = NumberUtils.toInt(getPara("country"));
		
		Operator operator = new Operator();
		operator.set("id", id);
		operator.set("name", name);
		operator.set("country", country);
		
		String msg = null;
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(operator);
				break;
			case PageHelp.OPER_UPDATE: 
				msg = update(operator);
				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(Operator operator){
		return operator.save()? "添加成功":"添加失败";
	}
	
	private String update(Operator operator){
		return operator.update()? "更新成功":"更新失败";
	}
	
}
