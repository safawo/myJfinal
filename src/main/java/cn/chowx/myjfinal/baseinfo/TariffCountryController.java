package cn.chowx.myjfinal.baseinfo;

import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.baseinfo.model.Coin;
import cn.chowx.myjfinal.baseinfo.model.Country;
import cn.chowx.myjfinal.baseinfo.model.Operator;
import cn.chowx.myjfinal.baseinfo.model.SpInfo;
import cn.chowx.myjfinal.baseinfo.model.TariffCountry;
import cn.chowx.myjfinal.common.BaseController;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 计费国家 controller
 * @author zcqshine
 *
 */
public class TariffCountryController extends BaseController {
	public void index(){
		String countryKeyValues = Country.DAO.getKeyValues();
		String spKeyValues = SpInfo.DAO.getKeyValues();
		String coinKeyValues = Coin.DAO.getSelectKeyValue();
		String operatorKeyValues = Operator.DAO.getSelectKeyValue();
		setAttr("countryKeyValues",countryKeyValues);
		setAttr("spKeyValues",spKeyValues);
		setAttr("coinKeyValues",coinKeyValues);
		setAttr("operatorKeyValues",operatorKeyValues);
		
		render("tariffCountry.html");
	}
	
	/**
	 * 分页获取计费国家信息
	 */
	public void list(){
		setPageHelp();
		Page<Record> page = TariffCountry.DAO.findRecordList(pageHelp);
		renderJsonData(page);
	}
	
	/**
	 * 增删改动作
	 */
	public void cud(){
		String oper = getPara("oper");
		
		int id = NumberUtils.toInt(getPara("id"));
		int spId = NumberUtils.toInt(getPara("spId"));
		int countryId = NumberUtils.toInt(getPara("countryId"));
		int coinId = NumberUtils.toInt(getPara("coinId"));
		int operatorId = NumberUtils.toInt(getPara("operatorId"));
		int limitTimesDay = NumberUtils.toInt(getPara("limitTimesDay"));
		int limitTimesMonth = NumberUtils.toInt(getPara("limitTimesMonth"));
		double tariff = NumberUtils.toDouble(getPara("tariff"));
		String cmdPort = getPara("cmdPort");
		int countType = NumberUtils.toInt(getPara("countType"));
		int status = NumberUtils.toInt(getPara("status"));
		
		TariffCountry tc = new TariffCountry();
		tc.set("id", id);
		tc.set("spId", spId);
		tc.set("countryId", countryId);
		tc.set("coinId", coinId);
		tc.set("operatorId", operatorId);
		tc.set("limitTimesDay", limitTimesDay);
		tc.set("limitTimesMonth", limitTimesMonth);
		tc.set("tariff", tariff);
		tc.set("cmdPort", cmdPort);
		tc.set("countType", countType);
		tc.set("status", status);
		String msg = null;
		
		if(!Strings.isNullOrEmpty(oper)){
			switch (oper) {
			case PageHelp.OPER_CREATE:
				msg = save(tc);
				break;
			case PageHelp.OPER_UPDATE: 
				msg = update(tc);
				break;
			case PageHelp.OPER_DELETE:
//				msg = delete(coin);
				break;
			default:
				break;
			}
		}
		renderJson(msg);
	}
	
	private String save(TariffCountry tc){
		tc.set("createTime", new Date());
		return tc.save() ? "添加成功" : "添加失败";
	}
	
	private String update(TariffCountry tc){
		return tc.update() ? "更新成功" : "更新失败";
	}
}
