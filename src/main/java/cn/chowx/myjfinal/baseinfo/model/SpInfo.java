package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;



/**
 * SP
 * @author zcqshine
 *
 */
public class SpInfo extends BaseModel<SpInfo> {
	private static final long serialVersionUID = -8891294564684491044L;
//	private static Logger log = Logger.getLogger(SpInfo.class);
	public static final SpInfo DAO = new SpInfo();
	
//	public Page<SpInfo> paginate(int pageNumber, int pageSize){
//		return paginate(pageNumber, pageSize, "select *", " from u_t_spInfos order by id ASC");
//	}
	
	/**
	 * 分页查询
	 * @param pageHelp
	 * @return
	 */
	public String findAll(PageHelp pageHelp){
		return getJqGridMap(pageHelp, TableName.SpInfo.getTableName());
	}
	
	/**
	 * 获取sp 信息的 id:spName 值对.形如 id:spName;id:spName;... 供 jqgrid 页面的下拉列表显示
	 * @return
	 */
	public String getKeyValues(){
		List<SpInfo> list = findList();
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(SpInfo sp : list){
				sb.append(sp.getInt("id")).append(":")
					.append(sp.getStr("spName")).append(";");
			}
			sb = sb.deleteCharAt(sb.lastIndexOf(";"));
		}
		return sb.toString();
	}
	
	/**
	 * 获取所有 sp 信息
	 * @return
	 */
	public List<SpInfo> findList(){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ").append(TableName.SpInfo.getTableName()).append(" order by `spStatus` desc");
		return find(sb.toString());
	}
}
