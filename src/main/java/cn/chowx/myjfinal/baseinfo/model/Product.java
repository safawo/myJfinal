package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 产品信息 model
 * @author zcqshine
 *
 */
public class Product extends BaseModel<Product> {
	private static final long serialVersionUID = -3249318107689309238L;
	public static final Product DAO = new Product();
	
	/**
	 * 分页查询产品信息, 返回 json 格式的数据,供 jqgrid 使用
	 * @param pageHelp
	 * @return
	 */
	public String findAll(PageHelp pageHelp){
		return getJqGridMap(pageHelp, TableName.Product.getTableName());
	}
	
	/**
	 * 获取产品信息的  id:name 值.  id:name;id:name;... 字符串
	 * @return
	 */
	public String getKeyValues(){
		List<Product> list = findList(1);
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(Product p : list){
				sb.append(p.getInt("id")).append(":")
					.append(p.getStr("name")).append(";");
			}
		}
		sb = sb.deleteCharAt(sb.lastIndexOf(";"));
		return sb.toString();
		
		
	}
	/**
	 * 根据状态获取产品列表. 1-有效, 0-无效
	 * @param status
	 * @return
	 */
	public List<Product> findList(int status){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM `").append(TableName.Product.getTableName()).append("` where status = "+status+"  ORDER BY `id` ASC");
		return find(sb.toString());
	}
}
