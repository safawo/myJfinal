package cn.chowx.myjfinal.baseinfo.model;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 计费国家
 * @author zcqshine
 *
 */
public class TariffCountry extends BaseModel<TariffCountry> {
	private static final long serialVersionUID = 4285463507264619352L;
	public static final TariffCountry DAO = new TariffCountry();
	
	public Page<Record> findRecordList(PageHelp pageHelp){
		String select = "select tariff.* ,country.name countryName, sp.spName, coin.name coinName, op.name operatorName";
		StringBuilder sb = new StringBuilder();
		sb.append("FROM ").append(TableName.TariffCountry.getTableName()).append(" tariff ")
			.append("LEFT JOIN ").append(TableName.Country.getTableName()).append(" country ON tariff.countryId = country.id ")
			.append("LEFT JOIN ").append(TableName.SpInfo.getTableName()).append(" sp ON tariff.spId = sp.id ")
			.append("LEFT JOIN ").append(TableName.Coin.getTableName()).append(" coin ON tariff.coinId = coin.id ")
			.append("LEFT JOIN ").append(TableName.Operator.getTableName()).append(" op ON tariff.operatorId = op.id ");
		sb.append(PageHelp.buildSearchSQL(pageHelp, "tariff"));
		sb.append(" order by ");
		if(!Strings.isNullOrEmpty(pageHelp.getSidx())){
			sb.append(" tariff.").append(pageHelp.getSidx()).append(" ").append(pageHelp.getSord());
		}else{
			sb.append(" tariff.status, tariff.countryId,tariff.spId,tariff.operatorId DESC");
		}
		Page<Record> records = Db.paginate(pageHelp.getPageNum(), pageHelp.getRows(), select, sb.toString());
		return records;
	}
}
