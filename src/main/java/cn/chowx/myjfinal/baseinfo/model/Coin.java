package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;

import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;
/**
 * 币种
 * @author zcqshine
 *
 */
public class Coin extends BaseModel<Coin> {

	private static final long serialVersionUID = -6162399136477622725L;
	public static final Coin DAO = new Coin();

	/**
	 * 分页查找币种信息
	 * @param pageHelp
	 * @return
	 */
	public Page<Record> findRecordList(PageHelp pageHelp){
		String select = "select A.*, B.name countryName";
		StringBuilder sb = new StringBuilder();
		sb.append("FROM ").append(TableName.Coin.getTableName()).append(" A,")
			.append(TableName.Country.getTableName()).append(" B")
			.append(" where A.country = B.id ");
		sb.append(PageHelp.buildSearchSQL(pageHelp, "A"));
		if(!Strings.isNullOrEmpty(pageHelp.getSidx())){
			sb.append(" order by ").append(pageHelp.getSidx()).append(" ").append(pageHelp.getSord());
		}else{
			sb.append(" order by A.name asc");
		}
		
		Page<Record> records = Db.paginate(pageHelp.getPageNum(), pageHelp.getRows(), select, sb.toString());
		return records;
	}
	
	/**
	 * 获取货币 id:name 值对. 形式如: id:name;id:name:... 供jqgrid下拉列表使用
	 * @return
	 */
	public String getSelectKeyValue(){
		List<Coin> list = findList();
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(Coin coin : list){
				sb.append(coin.getInt("id")).append(":")
					.append(coin.getStr("name")).append(";");
			}
			if(sb != null)
				sb = sb.deleteCharAt(sb.lastIndexOf(";"));
		}
		return sb.toString();
	}
	
	/**
	 * 获得所有货币信息
	 * @return
	 */
	public List<Coin> findList(){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ").append(TableName.Coin.getTableName());
		return find(sb.toString());
	}
	
}
