package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;
import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;


/**
 * 运营商
 * @author zcqshine
 *
 */
public class Operator extends BaseModel<Operator> {
	private static final long serialVersionUID = 2865945664116170295L;
	public static final Operator DAO = new Operator();
	
	/**
	 * 分页查找运营商信息
	 * @param pageHelp
	 * @return
	 */
	public Page<Record> findRecordList(PageHelp pageHelp){
		String select = "select A.*, B.name countryName";
		StringBuilder sb = new StringBuilder();
		sb.append("FROM ").append(TableName.Operator.getTableName()).append(" A, ")
			.append(TableName.Country.getTableName()).append(" B")
			.append(" where A.country = B.id");
		sb.append(PageHelp.buildSearchSQL(pageHelp, "A"));
		if(!Strings.isNullOrEmpty(pageHelp.getSidx())){
			sb.append(" order by A.").append(pageHelp.getSidx()).append(" ").append(pageHelp.getSord());
		}else{
			sb.append(" order by A.name asc");
		}
		
		Page<Record> records = Db.paginate(pageHelp.getPageNum(), pageHelp.getRows(), select, sb.toString());
		return records;
	}
	
	/**
	 * 获取 id:name 键值对. 形式如: id:name;id:name;... 供 jqGrid 下拉列表使用
	 * @return
	 */
	public String getSelectKeyValue(){
		List<Operator> list = findList();
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(Operator o : list){
				sb.append(o.getInt("id")).append(":")
					.append(o.getStr("name")).append(";");
			}
			sb = sb.deleteCharAt(sb.lastIndexOf(";"));
		}
		return sb.toString();
		
	}
	
	
	/**
	 * 获取所有运营商信息
	 * @return
	 */
	public List<Operator> findList(){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ").append(TableName.Operator.getTableName());
		return find(sb.toString());
	}
	
}
