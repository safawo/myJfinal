package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 国家/地区信息
 * @author zcqshine
 *
 */
public class Country extends BaseModel<Country> {
	private static final long serialVersionUID = 8229006932951062417L;
	public static final Country DAO = new Country();

	/**
	 * 分页查询国家信息
	 * @param pageHelp
	 * @return
	 */
	public String findAll(PageHelp pageHelp){
		return getJqGridMap(pageHelp, TableName.Country.getTableName());
	}
	
	/**
	 * 获取国家信息的id:name 值.    id:name;id:name;...字符串. 供下拉列表显示.
	 * @return
	 */
	public String getKeyValues(){
		List<Country> list = findList();
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(Country c : list){
				sb.append(c.getInt("id")).append(":")
					.append(c.getStr("name")).append(";");
			}
		}
		sb.append("0:无");
		return sb.toString();
	}
	
	/**
	 * 查询所有国家
	 * @return
	 */
	public List<Country> findList(){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM `").append(TableName.Country.getTableName()).append("` ORDER BY `name` ASC");
		return find(sb.toString());
	}
}
