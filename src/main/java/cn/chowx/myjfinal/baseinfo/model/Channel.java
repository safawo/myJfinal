package cn.chowx.myjfinal.baseinfo.model;

import java.util.List;

import cn.chowx.myjfinal.common.BaseModel;
import cn.chowx.myjfinal.common.TableName;
import cn.chowx.myjfinal.help.PageHelp;

/**
 * 渠道信息model
 * @author zcqshine
 *
 */
public class Channel extends BaseModel<Channel> {
	private static final long serialVersionUID = -632644435591965888L;
	public static final Channel DAO = new Channel();
	
	/**
	 * 分页查询渠道信息, 返回 json 字符串供 jqgrid 使用
	 * @param pageHelp
	 * @return
	 */
	public String findAll(PageHelp pageHelp){
		return getJqGridMap(pageHelp, TableName.Channel.getTableName());
	}
	
	/**
	 * 获取渠道信息的 id:name 值. 形如"id:name;id:name;..." 字符串
	 * @return
	 */
	public String getKeyValues(){
		List<Channel> list = findList(1);
		StringBuilder sb = new StringBuilder();
		if(list != null){
			for(Channel c : list){
				sb.append(c.getInt("id")).append(":")
					.append(c.getStr("name")).append(";");
			}
		}
		sb = sb.deleteCharAt(sb.lastIndexOf(";"));
		return sb.toString();
	}
	
	/**
	 * 根据状态获取渠道列表. 
	 * @param status 1-有效,0-无效
	 * @return
	 */
	public List<Channel> findList(int status){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM `").append(TableName.Channel.getTableName()).append("` where `status` = "+status+"  ORDER BY `id` ASC");
		return find(sb.toString());
	}
}
